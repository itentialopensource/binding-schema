import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { terser } from 'rollup-plugin-terser';

const filename = 'binding-schema.js';
const production = !process.env.ROLLUP_WATCH;

export default {
  input: 'src/index.js',
  output: [
    {
      file: `lib/${filename}`,
      format: 'cjs',
    },
    {
      file: `es/${filename}`,
      format: 'esm',
    },
    {
      name: 'BindingSchema',
      file: `umd/${filename}`,
      format: 'umd',
    },
  ],
  external: ['axios'],
  plugins: [
    resolve({
      main: true,
      browser: true,
    }),
    commonjs(),
    babel({
      runtimeHelpers: true,
      exclude: ['node_modules/**'],
      plugins: ['@babel/plugin-transform-runtime'],
      presets: [
        [
          '@babel/preset-env',
          {
            targets:
              'last 2 major versions and > 0.25%, ie 11, not op_mini all, not dead, maintained node versions',
          },
        ],
      ],
    }),
    production && terser(),
  ],
};
