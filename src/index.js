import binding from './utils/binding';

export default {
  async compose(bindingSchema, target = {}) {
    const bindingPaths = binding.uniqPaths(bindingSchema);

    const bindingConfigs = binding.configs(bindingPaths, bindingSchema);

    const bindingTargetPaths = binding.targetPaths(bindingPaths, bindingConfigs);

    const bindingRequests = binding.requests(bindingConfigs);

    const bindingResponses = await binding.responses(bindingRequests);

    const bindingSources = await binding.sources(bindingResponses, bindingConfigs, bindingPaths);

    return binding.insertToSchema(bindingTargetPaths, bindingSources, target).boundSchema;
  },
  async composeAll(bindingSchema, target = {}) {
    const bindingPaths = binding.uniqPaths(bindingSchema);

    const bindingConfigs = binding.configs(bindingPaths, bindingSchema);

    const bindingTargetPaths = binding.targetPaths(bindingPaths, bindingConfigs);

    const bindingRequests = binding.requests(bindingConfigs);

    const bindingResponses = await binding.responses(bindingRequests);

    const bindingSources = await binding.sources(bindingResponses, bindingConfigs, bindingPaths);

    return binding.insertToSchema(bindingTargetPaths, bindingSources, target);
  },
};
