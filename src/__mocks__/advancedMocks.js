const nestedConfigsMock = [
  {
    'binding:method': 'GET',
    'binding:link': {
      $ref: '/links',
      rel: 'collection',
    },
    'binding:source': {
      propertyPointer: '/users',
      keyPointer: '/name/first',
    },
    'binding:target': {
      propertyPointer: '/enum',
    },
    'binding:hyperSchema': {
      type: 'object',
      base: 'https://api.github.com/',
      links: [
        {
          rel: 'collection',
          href: 'users',
          targetMediaType: 'application/json',
          targetSchema: { $ref: '#' },
        },
      ],
    },
  },
];
const nestedKeyPathResMock = [
  {
    data: {
      users: [
        { name: { first: 'Darth', last: 'Vader' } },
        { name: { first: 'Luke', last: 'Skywalker' } },
      ],
    },
  },
];
const nestedSourceMock = [['Darth', 'Luke']];

const arrayConfigsMock = [
  {
    'binding:method': 'GET',
    'binding:link': {
      $ref: '/links',
      rel: 'collection',
    },
    'binding:source': {
      propertyPointer: '/',
      keyPointer: '/name/first',
    },
    'binding:target': {
      propertyPointer: '/enum',
    },
    'binding:hyperSchema': {
      type: 'object',
      base: 'https://api.github.com/',
      links: [
        {
          rel: 'collection',
          href: 'users',
          targetMediaType: 'application/json',
          targetSchema: { $ref: '#' },
        },
      ],
    },
  },
];
const arrayKeyPathResMock = [
  {
    data: [
      { name: { first: 'Darth', last: 'Vader' } },
      { name: { first: 'Luke', last: 'Skywalker' } },
    ],
  },
];
const arraySourceMock = [['Darth', 'Luke']];

export default {
  nestedConfigsMock,
  nestedKeyPathResMock,
  nestedSourceMock,
  arrayConfigsMock,
  arrayKeyPathResMock,
  arraySourceMock,
};
