const configsMock = [
  {
    'binding:method': 'GET',
    'binding:link': {
      $ref: '/links',
      rel: 'collection',
    },
    'binding:source': {
      propertyPointer: '/',
      keyPointer: '/login',
    },
    'binding:target': {
      propertyPointer: '/enum',
    },
    'binding:hyperSchema': {
      type: 'object',
      base: 'https://api.github.com/',
      links: [
        {
          rel: 'collection',
          href: 'users',
          targetMediaType: 'application/json',
          targetSchema: { $ref: '#' },
        },
      ],
    },
  },
  {
    'binding:method': 'GET',
    'binding:link': {
      $ref: '/links',
      rel: 'collection',
    },
    'binding:source': {
      propertyPointer: '/',
      keyPointer: '/name',
    },
    'binding:target': {
      propertyPointer: '/enum',
    },
    'binding:hyperSchema': {
      type: 'object',
      base: 'https://api.github.com/',
      links: [
        {
          rel: 'collection',
          href: 'repositories',
          targetMediaType: 'application/json',
          targetSchema: { $ref: '#' },
        },
      ],
    },
  },
];
const bindingSchemaMock = {
  properties: {
    user: configsMock[0],
    container: {
      properties: {
        repository: configsMock[1],
      },
    },
  },
};
const bindingSchemaAltMock = {
  properties: {
    container1: {
      properties: {
        dropdown2: {
          'binding:method': 'GET',
          'binding:timeout': 3000,
          'binding:link': {
            $ref: '/links',
            rel: 'collection',
          },
          'binding:source': {
            propertyPointer: '/results',
            keyPointer: '/name',
          },
          'binding:target': {
            propertyPointer: '/enum',
          },
          'binding:hyperSchema': {
            type: 'object',
            base: 'https://swapi.co/api',
            links: [
              {
                rel: 'collection',
                href: 'people',
                targetMediaType: 'application/json',
                targetSchema: {
                  $ref: '#',
                },
              },
            ],
          },
        },
      },
    },
  },
};
const bindingSchemaNonArrayMock = {
  count: {
    'binding:method': 'GET',
    'binding:link': {
      $ref: '/links',
      rel: 'collection',
    },
    'binding:source': {
      propertyPointer: '/count',
    },
    'binding:target': {
      propertyPointer: '/',
    },
    'binding:hyperSchema': {
      type: 'object',
      base: 'https://swapi.co/api',
      links: [
        {
          rel: 'collection',
          href: 'people',
          targetMediaType: 'application/json',
          targetSchema: {
            $ref: '#',
          },
        },
      ],
    },
  },
};
const pathsMock = [
  ['properties', 'user'],
  ['properties', 'container', 'properties', 'repository'],
];
const requestsMock = [
  {
    method: 'GET',
    baseURL: 'https://api.github.com/',
    url: 'users',
    headers: { 'Content-Type': 'application/json' },
  },
  {
    method: 'GET',
    baseURL: 'https://api.github.com/',
    url: 'repositories',
    headers: { 'Content-Type': 'application/json' },
  },
];
const emptySourcesMock = [];
const sourcesMock = [
  [
    'mojombo',
    'defunkt',
    'pjhyett',
    'wycats',
    'ezmobius',
    'ivey',
    'evanphx',
    'vanpelt',
    'wayneeseguin',
    'brynary',
    'kevinclark',
    'technoweenie',
    'macournoyer',
    'takeo',
    'caged',
    'topfunky',
    'anotherjesse',
    'roland',
    'lukas',
    'fanvsfan',
    'tomtt',
    'railsjitsu',
    'nitay',
    'kevwil',
    'KirinDave',
    'jamesgolick',
    'atmos',
    'errfree',
    'mojodna',
    'bmizerany',
  ],
  [
    'grit',
    'merb-core',
    'rubinius',
    'god',
    'jsawesome',
    'jspec',
    'exception_logger',
    'ambition',
    'restful-authentication',
    'attachment_fu',
    'microsis',
    's3',
    'taboo',
    'foxtracs',
    'fotomatic',
    'glowstick',
    'starling',
    'merb-more',
    'thin',
    'resource_controller',
    'markaby',
    'enum_field',
    'subtlety',
    'zippy',
    'cache_fu',
    'phosphor',
    'sinatra',
    'gsa-prototype',
    'duplikate',
    'lazy_record',
    'gsa-feeds',
    'votigoto',
    'mofo',
    'xhtmlize',
    'ruby-git',
    'bmhsearch',
    'mofo',
    'simply_versioned',
    'gchart',
    'schemr',
    'calais',
    'chronic',
    'git-wiki',
    'signal-wiki',
    'ruby-on-rails-tmbundle',
    'low-pro-for-jquery',
    'merb-core',
    'dst',
    'yaws',
    'yaws',
    'tasks',
    'ruby-on-rails-tmbundle',
    'amazon-ec2',
    'merblogger',
    'merbtastic',
    'alogr',
    'autozest',
    'rnginx',
    'sequel',
    'simply_versioned',
    'switchpipe',
    'arc',
    'ebay4r',
    'merb-plugins',
    'ram',
    'ambitious_activeldap',
    'fitter_happier',
    'oebfare',
    'credit_card_tools',
    'rorem',
    'braid',
    'uploadcolumn',
    'ruby-on-rails-tmbundle',
    'rack-mirror',
    'coset-mirror',
    'javascript-unittest-tmbundle',
    'eycap',
    'gitsum',
    'sequel-model',
    'god',
    'blerb-core',
    'django-mptt',
    'bus-scheme',
    'javascript-bits',
    'groomlake',
    'forgery',
    'ambitious-sphinx',
    'soup',
    'rails',
    'backpacking',
    'capsize',
    'starling',
    'ape',
    'awesomeness',
    'audited',
    'acts_as_geocodable',
    'acts_as_money',
    'calendar_builder',
    'clear_empty_attributes',
    'css_naked_day',
  ],
];
const targetPathsMock = [
  ['properties', 'user', 'enum'],
  ['properties', 'container', 'properties', 'repository', 'enum'],
];
const schemaMock = {
  type: 'object',
  title: 'Sample Form',
  properties: {
    user: {
      type: 'string',
      title: 'User',
      enum: [],
    },
    container: {
      type: 'object',
      properties: {
        repository: {
          type: 'string',
          title: 'Repository',
          enum: [],
        },
      },
    },
  },
};
const schemaAltMock = {
  title: 'Data binding',
  description: '',
  type: 'object',
  required: [],
  properties: {
    dropdown1: {
      type: 'string',
      title: 'Dropdown 1',
      description: '',
      enum: [],
    },
    container1: {
      type: 'object',
      title: 'Container 1',
      description: '',
      required: [],
      properties: {
        dropdown2: {
          type: 'string',
          title: 'Dropdown 2',
          description: '',
        },
      },
    },
    text1: {
      type: 'string',
      title: 'Text 1',
      description: '',
    },
  },
};
const boundSchemaEmptyMock = {
  boundSchema: {
    type: 'object',
    title: 'Sample Form',
    properties: {
      user: {
        type: 'string',
        title: 'User',
        enum: [],
      },
      container: {
        type: 'object',
        properties: {
          repository: {
            type: 'string',
            title: 'Repository',
            enum: [],
          },
        },
      },
    },
  },
  errors: [],
};
const boundSchemaMock = {
  boundSchema: {
    type: 'object',
    title: 'Sample Form',
    properties: {
      user: {
        type: 'string',
        title: 'User',
        enum: sourcesMock[0],
      },
      container: {
        type: 'object',
        properties: {
          repository: {
            type: 'string',
            title: 'Repository',
            enum: sourcesMock[1],
          },
        },
      },
    },
  },
  errors: [],
};
const boundSchemaAltMock = {
  title: 'Data binding',
  description: '',
  type: 'object',
  required: [],
  properties: {
    dropdown1: {
      type: 'string',
      title: 'Dropdown 1',
      description: '',
      enum: [],
    },
    container1: {
      type: 'object',
      title: 'Container 1',
      description: '',
      required: [],
      properties: {
        dropdown2: {
          type: 'string',
          title: 'Dropdown 2',
          description: '',
          enum: [
            'Luke Skywalker',
            'C-3PO',
            'R2-D2',
            'Darth Vader',
            'Leia Organa',
            'Owen Lars',
            'Beru Whitesun lars',
            'R5-D4',
            'Biggs Darklighter',
            'Obi-Wan Kenobi',
          ],
        },
      },
    },
    text1: {
      type: 'string',
      title: 'Text 1',
      description: '',
    },
  },
};
const boundSchemaNonArrayValueKeyMock = {
  count: 87,
};

export default {
  configsMock,
  bindingSchemaMock,
  bindingSchemaAltMock,
  bindingSchemaNonArrayMock,
  pathsMock,
  requestsMock,
  emptySourcesMock,
  sourcesMock,
  targetPathsMock,
  schemaMock,
  schemaAltMock,
  boundSchemaEmptyMock,
  boundSchemaMock,
  boundSchemaAltMock,
  boundSchemaNonArrayValueKeyMock,
};
