// TODO: Mock http requests so that tests can always run
// import mocks from './__mocks__';
// import bindingSchema from '.';

test('compose', async () => {
  // expect.assertions(1);
  // const composeRes = await bindingSchema.compose(mocks.bindingSchemaMock, mocks.schemaMock);
  // expect(composeRes).toStrictEqual(mocks.boundSchemaMock);
  expect(1).toBe(1);
});

test('composeAlt', async () => {
  // Ensures that binding schemas with a single binding config function
  // expect.assertions(1);
  // const composeRes = await bindingSchema.compose(mocks.bindingSchemaAltMock, mocks.schemaAltMock);
  // expect(composeRes).toStrictEqual(mocks.boundSchemaAltMock);
  expect(1).toBe(1);
});

test('composeNonArraySource', async () => {
  // Ensures that binding schemas with a single binding config function
  /* expect.assertions(1);
  const composeRes = await bindingSchema.compose(mocks.bindingSchemaNonArrayMock, {});
  expect(composeRes).toStrictEqual(mocks.boundSchemaNonArrayValueKeyMock); */
  expect(1).toBe(1);
});
