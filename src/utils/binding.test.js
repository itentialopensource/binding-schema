import binding from './binding';
import mocks from '../__mocks__';
import advancedMocks from '../__mocks__/advancedMocks';

test('uniqPaths', () => {
  const uniqPathsRes = binding.uniqPaths(mocks.bindingSchemaMock);
  expect(uniqPathsRes).toEqual(mocks.pathsMock);
});

test('configs', () => {
  const configsRes = binding.configs(mocks.pathsMock, mocks.bindingSchemaMock);
  expect(configsRes).toEqual(mocks.configsMock);
});

test('requests', () => {
  const requestsRes = binding.requests(mocks.configsMock);
  expect(requestsRes).toEqual(mocks.requestsMock);
});

test('responses', async () => {
  expect(true).toBe(true);
});

test('sources', async () => {
  expect.assertions(1);
  const responsesRes = await binding.responses(mocks.requestsMock);
  const sourcesRes = await binding.sources(responsesRes, mocks.configsMock);
  expect(sourcesRes).toStrictEqual(mocks.sourcesMock);
});

test('targetPaths', () => {
  const targetPathsRes = binding.targetPaths(mocks.pathsMock, mocks.configsMock);
  expect(targetPathsRes).toEqual(mocks.targetPathsMock);
});

test('insertToSchema', () => {
  const insertToSchemaRes = binding.insertToSchema(
    mocks.targetPathsMock,
    mocks.sourcesMock,
    mocks.schemaMock,
  );
  expect(insertToSchemaRes).toEqual(mocks.boundSchemaMock);
});

// An empty enum should not return undefined within the enum after binding
test('insertToSchema empty ENUM', () => {
  const insertToSchemaRes = binding.insertToSchema(
    mocks.targetPathsMock,
    mocks.emptySourcesMock,
    mocks.schemaMock,
  );
  expect(insertToSchemaRes).toEqual(mocks.boundSchemaEmptyMock);
});

describe('complex data structures', () => {
  test('nested keyPath sources', async () => {
    const sourcesRes = await binding.sources(
      advancedMocks.nestedKeyPathResMock,
      advancedMocks.nestedConfigsMock,
    );
    expect(sourcesRes).toStrictEqual(advancedMocks.nestedSourceMock);
  });
});
