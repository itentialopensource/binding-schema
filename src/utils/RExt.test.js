import * as R from 'ramda';
import RExt from './RExt';

test('flattenObj', () => {
  const source = { a: 1, b: { c: 2, d: 3 } };
  const target = { a: 1, 'b.c': 2, 'b.d': 3 };
  const flattenObjRes = RExt.flattenObj(source);
  expect(flattenObjRes).toEqual(target);
});

test('pathSplit', () => {
  const source = '/path/to/prop';
  const target = ['path', 'to', 'prop'];
  const pathSplitRes = RExt.pathSplit(source);
  expect(pathSplitRes).toEqual(target);
});

test('mapToPaths', () => {
  const source = { a: 1, 'b.c': 2, 'b.d': 3 };
  const target = [['a'], ['b', 'c'], ['b', 'd']];
  const mapToPathsRes = RExt.mapToPaths(source);
  expect(mapToPathsRes).toEqual(target);
});

test('mapToLenses', () => {
  const source = [['a'], ['b', 'c'], ['b', 'd']];
  const target = [R.lensPath(['a']), R.lensPath(['b', 'c']), R.lensPath(['b', 'd'])];
  const mapToLensesRes = RExt.mapToLenses(source);
  expect(target.length).toEqual(mapToLensesRes.length);
  expect(R.map(l => typeof l, mapToLensesRes)).toStrictEqual(R.map(l => typeof l, target));
  expect(R.map(l => l.toString(), mapToLensesRes)).toStrictEqual(R.map(l => l.toString(), target));
});

test('mapToViews', () => {
  const source = {
    lenses: [R.lensPath(['a']), R.lensPath(['b', 'c']), R.lensPath(['b', 'd'])],
    obj: { a: 1, b: { c: 2, d: 3 } },
  };
  const target = [1, 2, 3];
  const mapToViewsRes = RExt.mapToViews(source.lenses, source.obj);
  expect(mapToViewsRes).toEqual(target);
});

test('mergeDeepWithKeyAll', () => {
  const source = [{ a: 1 }, { b: 2 }, { container: { c: 3 } }];
  const target = { a: 1, b: 2, container: { c: 3 } };
  const mergeDeepWithKeyAllRes = RExt.mergeDeepWithKeyAll(R.concat, source);
  expect(mergeDeepWithKeyAllRes).toEqual(target);
});
