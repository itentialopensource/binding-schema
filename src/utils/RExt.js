import * as R from 'ramda';

export default {
  flattenObj: obj => {
    const go = obj_ =>
      R.chain(([k, v]) => {
        if (R.type(v) === 'Object' || R.type(v) === 'Array') {
          return R.map(([k_, v_]) => [`${k}.${k_}`, v_], go(v));
        }
        return [[k, v]];
      }, R.toPairs(obj_));

    return R.fromPairs(go(obj));
  },
  pathSplit: R.pipe(
    R.split('/'),
    R.filter(partial => partial.length > 0),
  ),
  mapToPaths: flattenedObj => R.map(key => key.split('.'), R.keys(flattenedObj)),
  mapToLenses: paths => R.map(p => R.lensPath(p), paths),
  mapToViews: (lenses, obj) => R.map(l => R.view(l, obj), lenses),
  mergeDeepWithKeyAll: (fn, list) =>
    R.reduce((acc, val) => R.mergeDeepWithKey(fn, acc, val), {}, list),
};
