import axios from 'axios';
import update from 'immutability-helper';
import * as R from 'ramda';
import RExt from './RExt';

const BINDING_KEY_REGEX = /^binding:.*/;
const COMBINATIONS = ['anyOf', 'allOf', 'oneOf'];
const errors = [];

const filterToBindingPaths = paths => {
  return R.filter(path => {
    const bindingKeyIndex = R.findIndex(R.test(BINDING_KEY_REGEX))(path);
    return bindingKeyIndex > -1;
  }, paths);
};
const mapToBindingPaths = paths => {
  return R.map(path => {
    const bindingKeyIndex = R.findIndex(R.test(BINDING_KEY_REGEX))(path);
    return R.dropLast(path.length - bindingKeyIndex, path);
  }, paths);
};
const mapToBindingRequest = config => {
  const method = config['binding:method'];
  const body = config['binding:body'];
  const hyperSchema = config['binding:hyperSchema'];
  const linkPath = RExt.pathSplit(config['binding:link'].$ref);
  const linksView = R.view(R.lensPath(linkPath), hyperSchema);
  const link = R.find(R.propEq('rel', config['binding:link'].rel), linksView);
  const timeout = config['binding:timeout'];

  return {
    method,
    timeout,
    baseURL: hyperSchema.base,
    url: link.href,
    ...(body && { data: body }),
    headers: {
      'Content-Type': link.targetMediaType,
    },
  };
};

const extractSource = config => async data => {
  if (config['binding:source'].transformation) {
    const transformedData = await axios
      .post(
        `/transformations/${config['binding:source'].transformation}`,
        Array.isArray(data)
          ? { incoming: { root: data } }
          : {
              incoming: data,
            },
      )
      .catch(err => err);

    if (!Array.isArray(transformedData.data) && typeof transformedData.data === 'object') {
      const [, dataValue] = Object.entries(transformedData.data)[0];
      return dataValue;
    }

    return transformedData.data;
  }
  const propertyPath = RExt.pathSplit(config['binding:source'].propertyPointer);
  const propertyLens = R.lensPath(propertyPath);
  let sourceView = R.view(propertyLens, data);

  if (
    config['binding:target'] &&
    (config['binding:target'].propertyPointer === '/enum' ||
      config['binding:target'].propertyPointer === '/enumNames') &&
    !sourceView
  ) {
    sourceView = [];
  }

  if (!config['binding:source'].keyPointer || config['binding:source'].keyPointer === '/') {
    return sourceView;
  }
  const keyPath = RExt.pathSplit(config['binding:source'].keyPointer);
  const keyLens = R.lensPath(keyPath);
  return R.map(obj => R.view(keyLens, obj), sourceView);
};

const handleResponses = (requests = []) => {
  // Remove request urls with undefined parameters e.g. /getCall/:id
  const filteredRequests = requests.filter(fR => {
    return !fR.url.includes(':');
  });

  return Promise.all(
    R.map(
      r =>
        axios
          .request(r)
          .then(
            response =>
              new Promise(resolve => {
                resolve(response);
              }),
          )
          .catch(
            error =>
              new Promise(resolve => {
                console.error(`Error: ${JSON.stringify(error)}`);
                resolve({
                  type: 'error',
                  message: error.message,
                });
              }),
          ),
      filteredRequests,
    ),
  );
};

export default {
  uniqPaths: R.pipe(
    RExt.flattenObj,
    RExt.mapToPaths,
    filterToBindingPaths,
    mapToBindingPaths,
    R.uniq,
  ),
  configs: (paths, schema) => RExt.mapToViews(RExt.mapToLenses(paths), schema),
  requests: bindings => R.map(b => mapToBindingRequest(b), bindings),
  responses: handleResponses,
  sources: async (responses, configs, paths) => {
    if (!responses || !responses.length) return [];
    let resConfigsPairs = R.zip(responses, configs);

    resConfigsPairs.forEach((resConfigPair, index) => {
      const response = R.head(resConfigPair);
      if (R.has('type', response) && response.type === 'error') {
        const errorObject = R.assocPath([paths[index]], response.message, {});
        errors.push(errorObject);
      }
    });

    // Filters out responses of requests that returned no data or error
    const isNotNull = resConfigPair => {
      const response = R.head(resConfigPair);
      return !(R.has('type', response) && response.type === 'error') || response !== undefined;
    };

    resConfigsPairs = R.filter(isNotNull, resConfigsPairs);

    return Promise.all(
      R.map(pair => {
        const response = R.head(pair);
        const config = R.last(pair);
        return extractSource(config)(response.data);
      }, resConfigsPairs),
    );
  },
  targetPaths: (paths, configs) => {
    const pathConfigPairs = R.zip(paths, configs);
    return R.map(pair => {
      const path = R.head(pair);
      const config = R.last(pair);
      const propertyPath = RExt.pathSplit(config['binding:target'].propertyPointer);
      return [...path, ...propertyPath];
    }, pathConfigPairs);
  },
  insertToSchema: (targetPaths, sources, schema) => {
    const bindings = {};

    targetPaths.forEach((targetPath, targetIndex) => {
      let stringPath = '';

      targetPath.forEach((path, pathIndex) => {
        const isCombination = COMBINATIONS.includes(targetPath[pathIndex - 1] || '');

        if (!(Number(path) || Number(path) === 0) || isCombination) {
          stringPath += path + (pathIndex !== targetPath.length - 1 ? ',' : '');
        }
      });

      if (
        Object.prototype.hasOwnProperty.call(bindings, stringPath) &&
        bindings[stringPath].targets
      ) {
        bindings[stringPath].targets.push(targetIndex);
      } else {
        bindings[stringPath] = {
          targets: [targetIndex],
          targetPath: stringPath.split(','),
        };
      }
    });

    const schemaUpdates = Object.values(bindings).map(binding => {
      const source = binding.targets.flatMap(targetIndex => {
        if (sources && sources.length > 0) return sources[targetIndex];
        return [];
      });

      if (binding.targets.length > 1) {
        const uniqueSource = [...new Set(source)];

        return R.assocPath(binding.targetPath, { $set: uniqueSource }, {});
      }

      return R.assocPath(binding.targetPath, { $set: source }, {});
    });

    const updates = RExt.mergeDeepWithKeyAll(
      R.concat,
      schemaUpdates.filter(el => el != null),
    );
    const response = update(schema, updates);
    return {
      boundSchema: response,
      errors,
    };
  },
};
