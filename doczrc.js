export default {
  title: 'Binding Schema',
  description:
    'A schema extension for JSON Schema and hyper schema to dynamically bind data into a schema document.',
  source: './docs',
  indexHtml: 'docs/index.html',
  base: '/binding-schema',
  public: 'docs/public',
  htmlContext: {
    favicon: 'public/favicon.ico',
  },
  themeConfig: {
    mode: 'light',
    colors: {
      primary: '#1B93D2',
      link: '#F37622',
      sidebarBg: '#212537',
      sidebarText: '#FFFFFF',
    },
  },
  menu: ['Introduction', 'Specification', 'Tools'],
};
