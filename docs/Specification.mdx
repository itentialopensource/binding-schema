---
name: Specification
route: /specification
---

# Binding Schema Specification

Binding Schema's specification aims to align itself with existing terminology used by the [JSON Schema](https://json-schema.org/latest/json-schema-core.html) specification.

## Binding Keys

JSON keys that are prefixed with `binding:` belong to the binding schema specification and act as instructions for a binding definition.

## Binding Definition

A binding definition contains the following properties:

- [binding:method](#bindingmethod)
- [binding:timeout](#bindingtimeout)
- [binding:link](#bindinglink)
- [binding:body](#bindingbody)
- [binding:source](#bindingsource)
- [binding:target](#bindingtarget)
- [binding:hyperSchema](#bindinghyperschema)

### binding:method

The method for the request that needs to be performed in order to retrieve data from a RESTful web service. Supported methods:

- GET
- POST

### binding:timeout
Defines a timeout for the request. If request is not returned within that time frame it will return an error.

### binding:link

Describes which link inside of a `binding:hyperschema` definition to execute a binding with.

#### binding:link.\$ref

A path inside of its hyper-schema definition at `binding:hyperschema`, in which the [link relation type](https://json-schema.org/latest/json-schema-hypermedia.html#relationType) of interest can be found. This follows the same format as [Schema Reference](https://json-schema.org/latest/json-schema-core.html#rfc.section.8.3).

#### binding:link.rel

A name to identify the specific [link relation](https://json-schema.org/latest/json-schema-hypermedia.html#rel) to be used by a binding definition.

### binding:body

###### (This property is conditional on `binding:method`)

This is the request body for a request. When `binding:method` is set GET, this property should not be defined; however, it is required when `binding:method` is set to POST.

### binding:source

The source is a JSON document that contains the data to be bound to a target. The `binding:source` object contains properties to describe binding instructions associated with the source.

#### binding:source.propertyPointer

A [JSON pointer](https://tools.ietf.org/html/draft-handrews-relative-json-pointer-01) that identifies the property by its key within the response data, where the source data can be found. (Often this is `/data` or `/results`.) The value of the property MUST be a valid [JSON Pointer](https://tools.ietf.org/html/rfc6901) in JSON String representation form.

#### binding:source.keyPointer

###### (This property is optional)

A key that identifies the sub-property of the source data that should be mapped to the target. This is used in situation where the response data is an array of objects. The value of the property MUST be a valid [JSON Pointer](https://tools.ietf.org/html/rfc6901) in JSON String representation form and is relative to the `binding:source.propertyPointer` location.

### binding:target

The target is a JSON document to which one or more sources are bound. The `binding:target` object contains properties to describe binding instructions associated with the target.

#### binding:target.propertyPointer

A [JSON pointer](https://tools.ietf.org/html/draft-handrews-relative-json-pointer-01) that identifies the property by its key within the target JSON document to which the source data should be bound. The value of the property MUST be a valid [JSON Pointer](https://tools.ietf.org/html/rfc6901) in JSON String representation form.

### binding:hyperSchema

Use the [JSON Hyper-Schema specification](https://json-schema.org/latest/json-schema-hypermedia.html) to define the hypermedia of interest. Required hyper-schema properties:

- [base](https://json-schema.org/latest/json-schema-hypermedia.html#rfc.section.5.1)
- [links](https://json-schema.org/latest/json-schema-hypermedia.html#rfc.section.5.2)

## Schema Structure & Nesting

Binding schemas allow defining any number of bindings on any level of an object.

For example, here is a binding schema document with three binding definitions at different levels of the JSON document.

```json
{
  "binding:method": "GET",
  "binding:link": {
    "$ref": "/links",
    "rel": "collection"
  },
  "binding:source": {
    "propertyPointer": "/data",
    "keyPointer": "/login"
  },
  "binding:target": {
    "propertyPointer": ""
  },
  "binding:hyperSchema": { ... },
  "users": {
    "binding:method": "GET",
    "binding:link": {
      "$ref": "/links",
      "rel": "collection"
    },
    "binding:source": {
      "propertyPointer": "/data",
      "keyPointer": "/login"
    },
    "binding:target": {
      "propertyPointer": ""
    },
    "binding:hyperSchema": { ... },
    "nestedUsers": {
      "binding:method": "GET",
      "binding:link": {
        "$ref": "/links",
        "rel": "collection"
      },
      "binding:source": {
        "propertyPointer": "/data",
        "keyPointer": "/login"
      },
      "binding:target": {
        "propertyPointer": ""
      },
      "binding:hyperSchema": { ... }
    }
  }
}
```
