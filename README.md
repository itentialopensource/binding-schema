# Binding Schema

A schema extension for JSON schema that builds upon JSON hyper-schema to dynamically bind data into a JSON schema document. This is the home for the binding schema specification and its official JavaScript library.

## Notice

This is an early version of a JSON schema extension.

## Upcoming Features of Spec:

### V1

- [ ] Support authentication via link.headerSchema
- [ ] Binding meta-schema

---

### V2

- [ ] Property dependencies for sequencing and internal/external references
- [ ] Source-to-Target data transformations
- [ ] JSON API Support?
- [ ] GraphQL Support?
- [ ] Further language support with binding schema libraries

## Upcoming Features of JavaScript library:

### V1

- [x] Support source.keyPointer as nested object
- [x] Support source.propertyPath as nested object
- [ ] Support target.propertyPath as nested object
- [ ] Support array usage within JSON pointers
- [ ] Support non-nested object bindings
- [x] Support keyless/non-array responses bindings
- [ ] Support authentication via link.headerSchema
- [ ] Binding schema validator
- [ ] Docs for how to use tool
- [ ] JSDocs in code
- [ ] Fix index.js unit tests

### V2

- [ ] Handle property dependencies
- [ ] Handle transformations
